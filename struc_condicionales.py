
clients = 'pablo,ricardo,'


def create_client(client_name):
    global clients # cojemos variable global clients

    if client_name not in clients:
        clients += client_name
        _add_comma()
    else:
        print('Client already is in the client\ 's list')

def _add_comma():
    global clients
    clients += ','


def list_clients():
    print(clients)


def _print_welcome():
    print('WELCOME TO MY PAGE')
    print('*'*50)
    print('what would you like to do today?')
    print('[C]reate a client')
    print('[D]elete a client')

if __name__ == '__main__':

    _print_welcome()
    command=input()

    if command == 'C':
        client_name=input('what is the client name?')
        create_client(client_name)
        list_clients( )
    elif command == 'D':
        pass
    else:
        print('Invalid command')
